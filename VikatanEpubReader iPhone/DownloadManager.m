//
//  DownloadManager.m
//  Findme
//
//  Created by Apple on 22/05/13.
//  Copyright (c) 2013 Apple. All rights reserved.
//

#import "DownloadManager.h"

@implementation DownloadManager
@synthesize responseData,dataDictionary,keyString;

-(void)startDownloadingFromURL:(NSString *)urlString delegate:(id)delegate :(NSString*)key
{
    callingclassObject = delegate;
    keyString = key;
    self.responseData = [[NSMutableData alloc]init];
    self.dataDictionary = [[NSMutableDictionary alloc]init];
    NSLog(@"URL String-->%@",urlString);
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [self.responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.responseData appendData:data];
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Connection Error");
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //NSLog(@"response data:%@",[NSJSONSerialization JSONObjectWithData:self.responseData options:NSJSONReadingMutableLeaves error:nil]);
    self.dataDictionary = [NSJSONSerialization JSONObjectWithData:self.responseData options:NSJSONReadingMutableLeaves error:nil];
    if([callingclassObject respondsToSelector:@selector(didFinishLoading:key:)])
            [callingclassObject didFinishLoading:self.dataDictionary key:self.keyString];
    
}
-(void)didFinishLoading:(NSDictionary*)responcedic key:(NSString*)key
{
    
}
+(void)showAlert:(NSString*)msg :(NSString*)title
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}
@end
