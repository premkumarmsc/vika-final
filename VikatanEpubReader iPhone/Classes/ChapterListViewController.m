//
//  ChapterListViewController.m
//  AePubReader
//
//  Created by ephronsystems on 9/10/13.
//
//

#import "ChapterListViewController.h"
#import <AnFengDe_EPUB_SDK/EPubSDKHeader.h>
@interface ChapterListViewController ()

@end

@implementation ChapterListViewController
@synthesize epubViewController;
NSMutableArray *array_list;
@synthesize table;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View lifecycle
- (NSString *) applicationDocumentsDirectory {
    return [NSString stringWithFormat: @"%@",
            [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex: 0]];
}
-(IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    

    
    
    
    NSString *cachePath = [NSString stringWithFormat:@"%@/Cache", [self applicationDocumentsDirectory]];
    int ret1 = [EPubONI initEPubEnv:cachePath];
    if (ret1 == AFD_EPUB_OK)
    {
        NSLog(@"Success");
    }
    else
        NSLog(@"FAiled");
    
    NSUserDefaults *passValues=[NSUserDefaults standardUserDefaults];
    //[passValues setObject:idArray[indexPath.row] forKey:@"BOOK_ID"];
    // [passValues setObject:epubArray[indexPath.row] forKey:@"BOOK_EPUB"];
    // [passValues setObject:textArray[indexPath.row] forKey:@"BOOK_NAME"];
    
    
    NSString *epubName=[NSString stringWithFormat:@"%@savedEpub.epub",[passValues objectForKey:@"BOOK_EPUB"]];
    
    NSUserDefaults *use=[NSUserDefaults standardUserDefaults];
    //[use setObject:userName forKey:@"DISPLAY_NAME"];
    
    NSString *user1Name=[use objectForKey:@"DISPLAY_NAME"];
    
    if (![user1Name isEqualToString:@""])
    {
        
        
        
        self.trackedViewName = [NSString stringWithFormat:@"%@-BookId:%@:Chapter List",user1Name,[passValues objectForKey:@"BOOK_EPUB"]];;
        
        
        
    }
    else
    {
        
         self.trackedViewName = [NSString stringWithFormat:@"%@-BookId:%@:Chapter List",@"Guest User",[passValues objectForKey:@"BOOK_EPUB"]];;
        
    }
    
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSArray *filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
    //NSLog(@"files array %@", filePathsArray);
    
    NSString *data_path=[passValues objectForKey:@"BOOK_EPUB"];
    
    
    
    
    NSString *compressed_data_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,epubName];
    
    // NSString *compressed_data_path=[NSString stringWithFormat:@"%@/%@.epub",documentsDirectory,@"savedEpub"];
    
    // savedEpub.epub
    
    NSLog(@"Compressed datapath:%@",compressed_data_path);
    
    
    
    
    array_list = [[NSMutableArray alloc] init];
    
    //    NSString *bundlePath = [[NSBundle mainBundle] resourcePath];
    //    NSString *epubFilePath = [bundlePath stringByAppendingString:epubName];
    //    NSLog(@"epubFilePath:%@",epubFilePath);
    //
    
    
    
    
    int ret = [EPubONI openEPubBook:compressed_data_path];
    if (ret > 0) {
        NSMutableArray *tmp = [[NSMutableArray alloc] init];
        [EPubONI getEPubChapter:tmp Handle:ret];
        int count = 0, count2 = 0;
        for (int i = 0; i < [tmp count]; i++) {
            EPubChapter *chapter = [tmp objectAtIndex:i];
            if (chapter.level == 1) {
                count ++;
                [array_list addObject:[NSString stringWithFormat:@"%d.%@",count,  chapter.title]];
                count2 = 0;
            }
            else if (chapter.level == 2) {
                count2++;
                [array_list addObject:[NSString stringWithFormat:@"    %d.%@",count2,  chapter.title]];
            }
        }
    }
    else
    {
        // [array_list addObject:[NSString stringWithFormat:@"error code is %d", ret]];
        [array_list addObject:[NSString stringWithFormat:@"Chapter 1"]];
        
    }

    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:array_list forKey:@"CHAPTERS"];
    
    
    // reloadData];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    
    NSLog(@"HELLO:%d",[epubViewController.loadedEpub.spineArray count]);
    return [epubViewController.loadedEpub.spineArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    cell.textLabel.numberOfLines = 2;
    cell.textLabel.lineBreakMode = UILineBreakModeMiddleTruncation;
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    
    NSLog(@"ARRAY LSIT:%@",array_list);
    
    @try {
        cell.textLabel.text = [array_list objectAtIndex:indexPath.row];
    }
    @catch (NSException *exception) {
        cell.textLabel.text = [NSString stringWithFormat:@"Chapter %d",indexPath.row+1];
    }
    
    
    
    //    NSString *chapter=[epubViewController.loadedEpub.spineArray[indexPath.row] title];
    //
    //    NSString *chapter1=[NSString stringWithFormat:@"%@",chapter];
    //
    //     NSLog(@"HELLO MADAM:%@",chapter1);
    //
    //
    //
    //    cell.textLabel.text = [[epubViewController.loadedEpub.spineArray objectAtIndex:[indexPath row]] title];
    //
    //   // cell.textLabel.text = chapter;
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [epubViewController loadSpine:[indexPath row] atPageIndex:0 highlightSearchResult:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
