//
//  ChapterListViewController.h
//  AePubReader
//
//  Created by ephronsystems on 9/10/13.
//
//

#import <UIKit/UIKit.h>
#import "EPubViewController.h"
@interface ChapterListViewController : GAITrackedViewController
{
EPubViewController* epubViewController;
}
@property(nonatomic,retain)IBOutlet UITableView *table;
@property(nonatomic, assign) EPubViewController* epubViewController;
- (NSString *) applicationDocumentsDirectory;
-(IBAction)back:(id)sender;
@end
