//
//  ViewController.m
//  VikatanEpub
//
//  Created by ephronsystems on 6/18/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "ViewController.h"
#import "EPubViewController.h"
#import "Image_cell.h"
#import "LibraryCell.h"
#import "WebViewController.h"
#import "SearchCell.h"
#import "ASIHTTPRequest.h"
#import "UIImageView+WebCache.h"
#import "ASIFormDataRequest.h"

#define RADIANS(degrees) ((degrees * M_PI) / 180.0)
@interface ViewController ()

@end

@implementation ViewController
@synthesize collectionView;
@synthesize bookmarkTable;
@synthesize topImageView,topView,storeButton,listButton,editButton,gridButton;
@synthesize searchTextField;
@synthesize searchView,searchTable;
@synthesize comingFromSync,syncBookID;
@synthesize threadProgressView;
@synthesize username,password;

@synthesize Activity;
UIAlertView *progressAlert;
int indexValueClick;

Image_cell *cell;
NSMutableArray *imageArray;
NSMutableArray *epubArray;
NSMutableArray *textArray;
NSMutableArray *idArray;


NSMutableArray *cidArray;
NSMutableArray *metadataArray;
NSMutableArray *isDownloadArray;
NSMutableArray *authorArray;
NSMutableArray *isDownloadebleArray;
NSMutableArray *preview_url_array;
NSMutableArray *isPreviewedArray;


NSMutableArray *searchStringArray;
NSMutableArray *searchIDArray;
NSMutableArray *searchImageArray;
NSMutableArray *searchEpubArray;

NSMutableArray *searchcidArray;
NSMutableArray *searchmetadataArray;
NSMutableArray *searchisDownloadArray;
NSMutableArray *searchauthorArray;
NSMutableArray *searcisDownloadebleArray;
NSMutableArray *searchpreview_url_array;
NSMutableArray *searchisPreviewedArray;

UIAlertView *delete_alert;

int editID;
int delIndex;

int downloading_id;

ASIHTTPRequest *request;
- (IBAction)CloseSearch:(id)sender
{
    searchView.hidden=YES;
    
    searchTextField.text=@"";
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
   
    
    
    threadProgressView.hidden=YES;
    downloading_id=12345;
    
    
    NSUserDefaults *use=[NSUserDefaults standardUserDefaults];
    //[use setObject:userName forKey:@"DISPLAY_NAME"];
    
    NSString *user1Name=[use objectForKey:@"DISPLAY_NAME"];
    NSString *dis=@"";
    if (![user1Name isEqualToString:@""])
    {
        
        
        
         self.trackedViewName = [NSString stringWithFormat:@"%@ - Library Page",user1Name];;
        
        dis=[NSString stringWithFormat:@"Welcome(%@)",user1Name];
        
    }
    else
    {
        
         self.trackedViewName = [NSString stringWithFormat:@"Guest user - Library Page"];;
        
        dis=[NSString stringWithFormat:@"Welcome(Guest user)"];
    }
    NSLog(@"HELLO COME:%@",dis);
    
    
    
    
    dis = [dis stringByReplacingOccurrencesOfString:@"%20"
                                         withString:@""];
    
    _bottomLabel.text=dis;

    
    
    [self.view addSubview:searchView];
    
    searchView.hidden=YES;
    
    searchView.frame=CGRectMake(0, 98, 320,466 );
    
    [gridButton setImage:[UIImage imageNamed:@"libGviewLandsACT.png"] forState:UIControlStateNormal];
     [listButton setImage:[UIImage imageNamed:@"libviewCLLands.png"] forState:UIControlStateNormal];
    
    
    
    editID=0;
     [self.collectionView registerNib:[UINib nibWithNibName:@"Image_cell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    
    
    if (![user1Name isEqualToString:@""])
    {
        
        bookmarkTable.hidden=YES;
        collectionView.hidden=NO;
        
        
    }
    else
    {
        bookmarkTable.hidden=YES;
        collectionView.hidden=YES;
        editButton.enabled=NO;
        gridButton.enabled=NO;
        listButton.enabled=NO;
    }

    
    [self.collectionView reloadData];
    
    
   // NSString *comingFromSync=@"NO";
    
    if ([comingFromSync isEqualToString:@"YES"])
    {
        [self parseJSON];
    }
    else
    {
        [self getFromDB];
    }
    
    
    
    
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)parseJSON
{
    NSLog(@"USER:%@",username);
    NSLog(@"PASSWORD:%@",password);
    
    
  
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.vikatan.com/fbook/index.php?apienckey=n21ak93"]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
        [request_post1234 setPostValue:@"purchased_books" forKey:@"po_type"];
     [request_post1234 setPostValue:username forKey:@"username"];
     [request_post1234 setPostValue:password forKey:@"pass"];
    
    
    
    NSUserDefaults *userde=[NSUserDefaults standardUserDefaults];
    [userde setObject:username forKey:@"USERNAME_EMAIL"];
    
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
      
         NSLog(@"XML:%@",responseString23);
        
        SBJSON *parser = [[SBJSON alloc] init];
        NSDictionary *statuses = [parser objectWithString:responseString23 error:nil];
        
       
        
        NSArray *Temp=[[statuses objectForKey:@"purchased_book"]valueForKey:@"book_list"] ;
        
        NSLog(@"XML:%@",Temp);
        
        
        
        NSArray *dbBookIdArr;
        NSArray *dbBookNameArr;
        NSArray *dbBookCIDArr;
        NSArray *dbBookAuthorArr;
        NSArray *dbBookPriceArr;
        NSArray *dbBookWrapperURLArr;
        NSArray *dbBookDownloadURLArr;
        NSArray *dbBookDOWNLOADABLEArr;
        NSArray *dbBookPreviewArr;
        NSArray *dbBookMetadata;
        
        
        
        for(NSDictionary *test in Temp)
        {
            dbBookIdArr =[test valueForKey:@"bid"];
            dbBookNameArr =[test valueForKey:@"b_name"];
            dbBookCIDArr =[test valueForKey:@"cid"];
            dbBookAuthorArr =[test valueForKey:@"author"];
            dbBookPriceArr =[test valueForKey:@"price"];
            dbBookWrapperURLArr =[test valueForKey:@"wrapper_url"];
            dbBookDownloadURLArr =[test valueForKey:@"download_url"];
            dbBookDOWNLOADABLEArr =[test valueForKey:@"downloadable"];
            dbBookPreviewArr =[test valueForKey:@"preview_url"];
            dbBookMetadata =[test valueForKey:@"metadata"];
            
            
            
        }
        
        NSUserDefaults *userde=[NSUserDefaults standardUserDefaults];
              NSString *dbUsername=[userde objectForKey:@"USERNAME_EMAIL"];
        
        NSString *querystring=[NSString stringWithFormat:@"select bid,b_name,cid,author,wrapper_url,download_url,downloadable,metadata,downloaded from tb_library where user_id='%@'",dbUsername];
        const char *sqlvalue = [querystring UTF8String];
        Dataware *dbsql=[[Dataware alloc]initDataware];
        sqlite3_stmt *sqlStmt=[dbsql OpenSQL:sqlvalue];
        NSMutableArray *checkArray=[[NSMutableArray alloc]init];
        
        if(sqlStmt != nil)
            while(sqlite3_step(sqlStmt) == SQLITE_ROW)
            {
                
                //NSLog(@"UU");
                
                NSString *id_str_val;
                @try {
                    id_str_val=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 0)];
                    
                }
                @catch (NSException *exception) {
                    id_str_val=@"";
                }
                
                [checkArray addObject:id_str_val];
                
                
                
            }
        
        NSLog(@"BOOK ID:%@",dbBookPreviewArr);
        
        
        
        //  syncBookID=@"244";
        
        NSLog(@"BOOK ID:%@",syncBookID);
        
        
        if ([checkArray indexOfObject:syncBookID] != NSNotFound)
        {
            // object found
            
            NSLog(@"FOUND");
            
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:nil message:@"Book is Already Synced" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            [alertView release];
        }
        else
        {
            // object not found
            NSLog(@"object not found");
            
            
            @try {
                for (int j=0; j<[dbBookIdArr count]; j++)
                {
                    
                    NSString *book_id_string=dbBookIdArr[j];
                    
                    NSLog(@"BOOK ID STR:%@",book_id_string);
                    
                    if ([book_id_string isEqualToString:syncBookID])
                    {
                        
                        NSUserDefaults *userde=[NSUserDefaults standardUserDefaults];
                        NSString *dbUsername=[userde objectForKey:@"USERNAME_EMAIL"];
                        
                        
                        Dataware *dd=[[Dataware alloc]initDataware];
                        
                        NSString *sstr=@"";
                        
                        sstr=@"INSERT INTO tb_library(bid,b_name,cid,author,price,wrapper_url,download_url,downloadable,preview_url,metadata,downloaded,previewed,user_id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
                        
                        
                        sqlite3_stmt *stmt=[dd OpenSQL:[sstr UTF8String]];
                        if (stmt!=nil)
                        {
                            sqlite3_bind_text(stmt,1,[dbBookIdArr[j] UTF8String],-1,SQLITE_TRANSIENT);
                            sqlite3_bind_text(stmt,2, [dbBookNameArr[j] UTF8String],-1 ,SQLITE_TRANSIENT);
                            sqlite3_bind_text(stmt,3, [dbBookCIDArr[j] UTF8String],-1,SQLITE_TRANSIENT);
                            sqlite3_bind_text(stmt,4, [dbBookAuthorArr[j] UTF8String],-1,SQLITE_TRANSIENT);
                            sqlite3_bind_text(stmt,5, [dbBookPriceArr[j] UTF8String],-1,SQLITE_TRANSIENT);
                            sqlite3_bind_text(stmt,6, [dbBookWrapperURLArr[j] UTF8String],-1,SQLITE_TRANSIENT);
                            sqlite3_bind_text(stmt,7,[dbBookDownloadURLArr[j] UTF8String],-1,SQLITE_TRANSIENT);
                            sqlite3_bind_text(stmt,8, [dbBookDOWNLOADABLEArr[j] UTF8String],-1 ,SQLITE_TRANSIENT);
                            sqlite3_bind_text(stmt,9, [dbBookPreviewArr[j] UTF8String],-1,SQLITE_TRANSIENT);
                            sqlite3_bind_text(stmt,10, [dbBookMetadata[j] UTF8String],-1,SQLITE_TRANSIENT);
                            sqlite3_bind_text(stmt,11, [@"NO" UTF8String],-1,SQLITE_TRANSIENT);
                            sqlite3_bind_text(stmt,12, [@"NO" UTF8String],-1,SQLITE_TRANSIENT);
                            sqlite3_bind_text(stmt,13, [dbUsername UTF8String],-1,SQLITE_TRANSIENT);
                            
                            sqlite3_step(stmt);
                        }
                        sqlite3_finalize(stmt);
                    }
                    
                }
            }
            @catch (NSException *exception) {
                
            }
           
           
            
            
            
        }
        
        [self getFromDB];

                
        
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
    
    
//     NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.vikatan.com/fbook/index.php?apienckey=n21ak93&module=fbook&type=purchased_book&email_id=%@&password=%@",username,password]];    
//    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
//    [request setDelegate:self];
//    [request startAsynchronous];
}

- (void)requestFinished:(ASIHTTPRequest *)request1
{
    
        NSString *responseString = [request1 responseString];
        
        
       
        SBJSON *parser = [[SBJSON alloc] init];
     NSDictionary *statuses = [parser objectWithString:responseString error:nil];
    
    NSLog(@"XML:%@",statuses);
    
     NSArray *Temp=[[statuses objectForKey:@"purchased_book"]valueForKey:@"book_list"] ;
    
   NSLog(@"XML:%@",Temp);
    

    
    NSArray *dbBookIdArr;
    NSArray *dbBookNameArr;
    NSArray *dbBookCIDArr;
    NSArray *dbBookAuthorArr;
    NSArray *dbBookPriceArr;
    NSArray *dbBookWrapperURLArr;
    NSArray *dbBookDownloadURLArr;
    NSArray *dbBookDOWNLOADABLEArr;
    NSArray *dbBookPreviewArr;
     NSArray *dbBookMetadata;
    
    
    
    for(NSDictionary *test in Temp)
    {
        dbBookIdArr =[test valueForKey:@"bid"];
        dbBookNameArr =[test valueForKey:@"b_name"];
        dbBookCIDArr =[test valueForKey:@"cid"];
        dbBookAuthorArr =[test valueForKey:@"author"];
        dbBookPriceArr =[test valueForKey:@"price"];
        dbBookWrapperURLArr =[test valueForKey:@"wrapper_url"];
        dbBookDownloadURLArr =[test valueForKey:@"download_url"];
        dbBookDOWNLOADABLEArr =[test valueForKey:@"downloadable"];
        dbBookPreviewArr =[test valueForKey:@"preview_url"];
         dbBookMetadata =[test valueForKey:@"metadata"];
        
        
        
    }
    
    
    NSUserDefaults *userde=[NSUserDefaults standardUserDefaults];
    NSString *dbUsername=[userde objectForKey:@"USERNAME_EMAIL"];
    
    NSString *querystring=[NSString stringWithFormat:@"select bid,b_name,cid,author,wrapper_url,download_url,downloadable,metadata,downloaded from tb_library where user_id='%@'",dbUsername];
    
     const char *sqlvalue = [querystring UTF8String];
    Dataware *dbsql=[[Dataware alloc]initDataware];
	sqlite3_stmt *sqlStmt=[dbsql OpenSQL:sqlvalue];
    NSMutableArray *checkArray=[[NSMutableArray alloc]init];
    
	if(sqlStmt != nil)
		while(sqlite3_step(sqlStmt) == SQLITE_ROW)
		{
            
            //NSLog(@"UU");
            
            NSString *id_str_val;
            @try {
                id_str_val=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 0)];
                
            }
            @catch (NSException *exception) {
                id_str_val=@"";
            }
                        
            [checkArray addObject:id_str_val];
            
            
            
        }
    
    NSLog(@"BOOK ID:%@",dbBookPreviewArr);
    
    
    
  //  syncBookID=@"244";
       
    NSLog(@"BOOK ID:%@",syncBookID);
    
    
    if ([checkArray indexOfObject:syncBookID] != NSNotFound)
    {
        // object found
        
        NSLog(@"FOUND");
        
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:nil message:@"Book is Already Synced" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        [alertView release];
    }
    else
    {
        // object not found
         NSLog(@"object not found");
        
        for (int j=0; j<[dbBookIdArr count]; j++)
        {
            
            NSString *book_id_string=dbBookIdArr[j];
            
            NSLog(@"BOOK ID STR:%@",book_id_string);
            
            if ([book_id_string isEqualToString:syncBookID])
            {
                
                Dataware *dd=[[Dataware alloc]initDataware];
                
                NSUserDefaults *userde=[NSUserDefaults standardUserDefaults];
                NSString *dbUsername=[userde objectForKey:@"USERNAME_EMAIL"];
                
                
                NSString *sstr=@"";
                
                sstr=@"INSERT INTO tb_library(bid,b_name,cid,author,price,wrapper_url,download_url,downloadable,preview_url,metadata,downloaded,previewed,user_id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
                
                
                sqlite3_stmt *stmt=[dd OpenSQL:[sstr UTF8String]];
                if (stmt!=nil)
                {
                    sqlite3_bind_text(stmt,1,[dbBookIdArr[j] UTF8String],-1,SQLITE_TRANSIENT);
                    sqlite3_bind_text(stmt,2, [dbBookNameArr[j] UTF8String],-1 ,SQLITE_TRANSIENT);
                    sqlite3_bind_text(stmt,3, [dbBookCIDArr[j] UTF8String],-1,SQLITE_TRANSIENT);
                    sqlite3_bind_text(stmt,4, [dbBookAuthorArr[j] UTF8String],-1,SQLITE_TRANSIENT);
                    sqlite3_bind_text(stmt,5, [dbBookPriceArr[j] UTF8String],-1,SQLITE_TRANSIENT);
                    sqlite3_bind_text(stmt,6, [dbBookWrapperURLArr[j] UTF8String],-1,SQLITE_TRANSIENT);
                    sqlite3_bind_text(stmt,7,[dbBookDownloadURLArr[j] UTF8String],-1,SQLITE_TRANSIENT);
                    sqlite3_bind_text(stmt,8, [dbBookDOWNLOADABLEArr[j] UTF8String],-1 ,SQLITE_TRANSIENT);
                    sqlite3_bind_text(stmt,9, [dbBookPreviewArr[j] UTF8String],-1,SQLITE_TRANSIENT);
                    sqlite3_bind_text(stmt,10, [dbBookMetadata[j] UTF8String],-1,SQLITE_TRANSIENT);
                    sqlite3_bind_text(stmt,11, [@"NO" UTF8String],-1,SQLITE_TRANSIENT);
                     sqlite3_bind_text(stmt,12, [@"NO" UTF8String],-1,SQLITE_TRANSIENT);
                     sqlite3_bind_text(stmt,13, [dbUsername UTF8String],-1,SQLITE_TRANSIENT);
                    
                    sqlite3_step(stmt);
                }
                sqlite3_finalize(stmt);
            }
            
        }
        
        
        
    }
     
   [self getFromDB];
    
    
       
    
   
   

}
-(void)getFromDB
{
    
    
   
    
    imageArray=[[NSMutableArray alloc]init];
    epubArray=[[NSMutableArray alloc]init];
    textArray=[[NSMutableArray alloc]init];
    idArray=[[NSMutableArray alloc]init];
    cidArray=[[NSMutableArray alloc]init];
    metadataArray=[[NSMutableArray alloc]init];
    isDownloadArray=[[NSMutableArray alloc]init];
    authorArray=[[NSMutableArray alloc]init];
    isDownloadebleArray=[[NSMutableArray alloc]init];
    preview_url_array=[[NSMutableArray alloc]init];
    isPreviewedArray=[[NSMutableArray alloc]init];
    
    NSLog(@"ENTER");
    
    NSUserDefaults *userde=[NSUserDefaults standardUserDefaults];
    NSString *dbUsername=[userde objectForKey:@"USERNAME_EMAIL"];
    
    NSString *querystring=[NSString stringWithFormat:@"select bid,b_name,cid,author,wrapper_url,download_url,downloadable,metadata,downloaded,preview_url,previewed from tb_library where user_id='%@'",dbUsername];
    
    const char *sqlvalue = [querystring UTF8String];
    
    NSLog(@"SQL VALUE:%s",sqlvalue);
    
    Dataware *dbsql=[[Dataware alloc]initDataware];
	sqlite3_stmt *sqlStmt=[dbsql OpenSQL:sqlvalue];
    
    
	if(sqlStmt != nil)
		while(sqlite3_step(sqlStmt) == SQLITE_ROW)
		{
            
            //NSLog(@"UU");
            
            NSString *id_str;
            @try {
                id_str=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 0)];
                
            }
            @catch (NSException *exception) {
                id_str=@"";
            }
            
            
            NSString *name_str;
            @try {
                name_str=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 1)];
                
            }
            @catch (NSException *exception) {
                name_str=@"";
            }
            
            
            NSString *cid_str;
            @try {
                cid_str=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 2)];
                
            }
            @catch (NSException *exception) {
                cid_str=@"";
            }
            
            
            NSString *author_str;
            @try {
                author_str=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 3)];
                
            }
            @catch (NSException *exception) {
                author_str=@"";
            }
            
            
            NSString *wrapper_url_str;
            @try {
                wrapper_url_str=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 4)];
                
            }
            @catch (NSException *exception) {
                wrapper_url_str=@"";
            }
            
            
            NSString *download_url_str;
            @try {
                download_url_str=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 5)];
                
            }
            @catch (NSException *exception) {
                download_url_str=@"";
            }
            
            NSString *downloadable_str;
            @try {
                downloadable_str=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 6)];
                
            }
            @catch (NSException *exception) {
                downloadable_str=@"";
            }
            
            NSString *metadata_str;
            @try {
                metadata_str=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 7)];
                
            }
            @catch (NSException *exception) {
                metadata_str=@"";
            }
            
            NSString *downloaded_str;
            @try {
                downloaded_str=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 8)];
                
            }
            @catch (NSException *exception) {
                downloaded_str=@"";
            }
            
            NSString *preview_str;
            @try {
                preview_str=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 9)];
                
            }
            @catch (NSException *exception) {
                preview_str=@"";
            }
            
            NSString *ispreview_str;
            @try {
                ispreview_str=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 10)];
                
            }
            @catch (NSException *exception) {
                ispreview_str=@"";
            }
            
            [idArray addObject:id_str];
            [textArray addObject:name_str];
            [cidArray addObject:cid_str];
            [authorArray addObject:author_str];
            [imageArray addObject:wrapper_url_str];
            [epubArray addObject:download_url_str];
            [isDownloadebleArray addObject:downloadable_str];
            [metadataArray addObject:metadata_str];
            [isDownloadArray addObject:downloaded_str];
            [preview_url_array addObject:preview_str];
             [isPreviewedArray addObject:ispreview_str];
            
            
        }
      
    
    
    
    
    
    
    
    [dbsql CloseSQL];
    
    
    NSLog(@"HELLO COUNT:%@",epubArray);
    NSLog(@"HELLO COUNT:%@",preview_url_array);
    
    
    if ([comingFromSync isEqualToString:@"YES"])
    {
    
    if ([epubArray count]!=0)
    {
        
        NSLog(@"BEFORE");
    
        int download_id=0;
    
         NSLog(@"AFTER");
        
    for (int i=0; i<[idArray count]; i++)
    
    {
        
        if ([idArray[i] intValue]==[syncBookID intValue])
        {
            NSLog(@"ID VALUE:%d",i);
            download_id=i;
        }
        
        
        
    }
        NSString *send_book_id;
        NSString *send_book_name;
        NSString *epubFileDownloadURL;
        @try {
            send_book_id=[NSString stringWithFormat:@"%@",idArray[download_id]];
            send_book_name=[NSString stringWithFormat:@"%@",textArray[download_id]];
            epubFileDownloadURL=[NSString stringWithFormat:@"%@",epubArray[download_id]];
            
            
            
            // epubFileDownloadURL=@"http://api.vikatan.com/test/download/2000.epub";
            
            NSLog(@"BOOK ID:%@",send_book_id);
            NSLog(@"BOOK NAME:%@",send_book_name);
            NSLog(@"BOOK URL:%@",epubFileDownloadURL);
        }
        @catch (NSException *exception) {
            send_book_id=[NSString stringWithFormat:@"%@",idArray[0]];
            send_book_name=[NSString stringWithFormat:@"%@",textArray[0]];
            epubFileDownloadURL=[NSString stringWithFormat:@"%@",epubArray[0]];
            
            
            
            // epubFileDownloadURL=@"http://api.vikatan.com/test/download/2000.epub";
            
            NSLog(@"BOOK ID:%@",send_book_id);
            NSLog(@"BOOK NAME:%@",send_book_name);
            NSLog(@"BOOK URL:%@",epubFileDownloadURL);
        }
        

    
    
    NSUserDefaults *passValues=[NSUserDefaults standardUserDefaults];
    [passValues setObject:send_book_id forKey:@"BOOK_ID"];
    [passValues setObject:send_book_id forKey:@"BOOK_EPUB"];
    [passValues setObject:send_book_name forKey:@"BOOK_NAME"];
    
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *epub_name=[NSString stringWithFormat:@"%@.epub",send_book_id];
    NSString* foofile = [documentsPath stringByAppendingPathComponent:epub_name];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
    
    
    if (fileExists)
    {
               
        
        
    }
    else
    {
        
        
        if ([comingFromSync isEqualToString:@"YES"])
        {
        
        NSLog(@"NOT EXISTS");
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
                
        downloading_id=download_id;
        
        // threadProgressView.hidden=NO;
        
        
        
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSURL *url=[NSURL URLWithString:epubFileDownloadURL];
        
        
        NSLog(@"Dowload Start");
        
        request = [ASIHTTPRequest requestWithURL:url];
        
        [self.collectionView reloadData];
        [bookmarkTable reloadData];
        
        
        //  [collectionView reloadData];
        
        [request setShouldContinueWhenAppEntersBackground:YES];
         [request setTimeOutSeconds:240];
        
        [request setCompletionBlock:^{
            // Use when fetching text data
            
            NSString *responseString = [request responseString];
            
            
            
            // Use when fetching binary data
            NSData *responseData = [request responseData];
            
            NSString *epub_name=[NSString stringWithFormat:@"%@.epub",send_book_id];
            
            
            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:epub_name];
            
            [responseData writeToFile:filePath atomically:YES];
            
            NSLog(@"Dowload Completes");
            
            threadProgressView.hidden=YES;
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             downloading_id=12345;
            // [self Load:nil];
            
            
            [bookmarkTable reloadData];
            [collectionView reloadData];
                       
            
        }];
        [request setFailedBlock:^
         {
             // NSError *error = [request error];
             
             
             UIAlertView *error_Alert=[[UIAlertView alloc]initWithTitle:@"Download Error" message:[NSString stringWithFormat:@"%@",[request error]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             
             [error_Alert show];
             
             
         }];
        [request startAsynchronous];
        
        
        }
    }

    
   
        
    }
        
    }
    
    
    if ([idArray count]==0)
    {
        UIAlertView *errorAlert = [[[UIAlertView alloc] initWithTitle:@"விகடன்"
                                                              message:@"There are no books available in your library. Please purchase a book and sync"
                                                             delegate:self
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil] autorelease];
        
        [errorAlert show];
        
    }
    
    NSLog(@"REACHED");
    [bookmarkTable reloadData];
    [collectionView reloadData];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [idArray count];
}


- (Image_cell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    
    
    
    if (editID==0)
    {
        cell.deletebtn.hidden=YES;
         //cell.image_view.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",imageArray[indexPath.row]]];
    }
    else
    {
        
        [cell.deletebtn setTag:indexPath.row];
        cell.deletebtn.hidden=NO;
        [cell.deletebtn addTarget:self action:@selector(deletebuttonClicked:)
                      forControlEvents:UIControlEventTouchDown];
        
        
         //cell.image_view.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",imageArray[indexPath.row]]];
        
                
    }
    
    [cell.image_view setImageWithURL:[NSURL URLWithString:imageArray[indexPath.row]]
                 placeholderImage:[UIImage imageNamed:@"temp.jpg"]];
    
    if (downloading_id==12345)
    {
        cell.progress.hidden=YES;
    }
    else
    {
        if (downloading_id==indexPath.row)
        {
            cell.progress.hidden=NO;
            [request setDownloadProgressDelegate:cell.progress];
        }
        else
        {
            cell.progress.hidden=YES;
            //[request setDownloadProgressDelegate:cell.progress];
        }
        
       
        
    }
    
   // cell.progress.hidden=YES;
    
     
    
   //cell.image_view.image=[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@", image_path]];
    
    return cell;
}

-(void)deletebuttonClicked:(UIButton*)button
{
    
    //////NSLog(@"update Button %ld clicked.",(long int)[button tag]);
    
  
    
    NSLog(@"DELETE INDEX:%ld",(long int)[button tag]);
    
    
    delIndex=(long int)[button tag];
    
    
    delete_alert = [[[UIAlertView alloc] initWithTitle:@"Really Delete?"
                                               message:@"Do you really want to delete this Book?"
                                              delegate:self
                                     cancelButtonTitle:@"Cancel"
                                     otherButtonTitles:@"Yes",nil] autorelease];
    
    [delete_alert show];

    
    
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"Select %d",indexPath.row);
    
    if (editID==1)
    {
        NSLog(@"CONFIRM DELETE");
        
         delIndex=indexPath.row;
        
        delete_alert = [[[UIAlertView alloc] initWithTitle:@"Really Delete?"
                                                   message:@"Do you really want to delete this Book?"
                                                  delegate:self
                                         cancelButtonTitle:@"Cancel"
                                         otherButtonTitles:@"Yes",nil] autorelease];
        
        [delete_alert show];

    }
    else
    {
    
        indexValueClick=indexPath.row;
        
        progressAlert = [[UIAlertView alloc] initWithTitle: @"Loading..." message: @"Please wait..." delegate: self cancelButtonTitle: nil otherButtonTitles: nil];
        
        Activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(90.0f, 40.0f, 100.0f, 100.0f)];
        
        
        
        [progressAlert addSubview:Activity];
        
        [Activity startAnimating];
        
        [progressAlert show];
        [progressAlert release];
        
         [self performSelector:@selector(goto_next1) withObject:nil afterDelay:1];
   
        
         
    }
   
}

-(void)goto_next1
{

    
    NSString *send_book_id=[NSString stringWithFormat:@"%@",idArray[indexValueClick]];
    NSString *send_book_name=[NSString stringWithFormat:@"%@",textArray[indexValueClick]];
    NSString *epubFileDownloadURL=[NSString stringWithFormat:@"%@",epubArray[indexValueClick]];
    
    
    NSLog(@"BOOK ID:%@",send_book_id);
    NSLog(@"BOOK NAME:%@",send_book_name);
    NSLog(@"BOOK URL:%@",epubFileDownloadURL);
    
    
    NSUserDefaults *passValues=[NSUserDefaults standardUserDefaults];
    [passValues setObject:send_book_id forKey:@"BOOK_ID"];
    [passValues setObject:send_book_id forKey:@"BOOK_EPUB"];
    [passValues setObject:send_book_name forKey:@"BOOK_NAME"];
    
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *epub_name=[NSString stringWithFormat:@"%@.epub",send_book_id];
    NSString* foofile = [documentsPath stringByAppendingPathComponent:epub_name];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
    
    
    if (fileExists)
    {
        NSLog(@"EXISTS");
        
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            CGSize result = [[UIScreen mainScreen] bounds].size;
            if(result.height == 480)
            {
                // iPhone Classic
                
                NSLog(@"OLD IPHONE");
                
                EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EpubView~iPhone" bundle:nil];
                
                //epub.epubName=@"7";
                
                epub.epubName=send_book_id;
                
                [self presentModalViewController:epub animated:NO];
                
            }
            if(result.height == 568)
            {
                // iPhone 5
                EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EpubView~iPhone5" bundle:nil];
                
                //epub.epubName=@"7";
                
                epub.epubName=send_book_id;
                
                [self presentModalViewController:epub animated:NO];
            }
            
            [Activity stopAnimating];
            [progressAlert dismissWithClickedButtonIndex:0 animated:YES];

        }
        else
        {
            NSLog(@"iPAd");
            
            
            
            
            EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EPubView" bundle:nil];
            epub.epubName=send_book_id;
            // epub.epubName=@"7777";
            
            [self presentModalViewController:epub animated:NO];
        }
        
        
        
        
    }
    else
    {
        
        NSLog(@"NOT EXISTS");
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
              
        downloading_id=indexValueClick;
        
        // threadProgressView.hidden=NO;
        
        
        
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSURL *url=[NSURL URLWithString:epubFileDownloadURL];
        
        
        NSLog(@"Dowload Start");
        
        request = [ASIHTTPRequest requestWithURL:url];
        
        [self.collectionView reloadData];
        [bookmarkTable reloadData];
        
        
        //  [collectionView reloadData];
        
        [request setShouldContinueWhenAppEntersBackground:YES];
         [request setTimeOutSeconds:240];
        
        [request setCompletionBlock:^{
            // Use when fetching text data
            
            NSString *responseString = [request responseString];
            
            
            
            // Use when fetching binary data
            NSData *responseData = [request responseData];
            
            NSString *epub_name=[NSString stringWithFormat:@"%@.epub",send_book_id];
            
            
            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:epub_name];
            
            [responseData writeToFile:filePath atomically:YES];
            
            NSLog(@"Dowload Completes");
            
            threadProgressView.hidden=YES;
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            // [self Load:nil];
            
            
            NSUserDefaults *passValues=[NSUserDefaults standardUserDefaults];
            [passValues setObject:send_book_id forKey:@"BOOK_ID"];
            [passValues setObject:send_book_id forKey:@"BOOK_EPUB"];
            [passValues setObject:send_book_name forKey:@"BOOK_NAME"];
            
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.height == 480)
                {
                    // iPhone Classic
                    
                    NSLog(@"OLD IPHONE");
                    
                    EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EpubView~iPhone" bundle:nil];
                    
                    //epub.epubName=@"7";
                    
                    epub.epubName=send_book_id;
                    
                    [self presentModalViewController:epub animated:NO];
                }
                if(result.height == 568)
                {
                    // iPhone 5
                    EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EpubView~iPhone5" bundle:nil];
                    
                    //epub.epubName=@"7";
                    
                    epub.epubName=send_book_id;
                    
                    [self presentModalViewController:epub animated:NO];            }
            }
            else
            {
                NSLog(@"iPAd");
                
                EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EPubView" bundle:nil];
                epub.epubName=send_book_id;
                // epub.epubName=@"7777";
                
                [self presentModalViewController:epub animated:NO];
            }
            [Activity stopAnimating];
            [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
            
        }];
        [request setFailedBlock:^
         {
             // NSError *error = [request error];
             
             
             UIAlertView *error_Alert=[[UIAlertView alloc]initWithTitle:@"Download Error" message:[NSString stringWithFormat:@"%@",[request error]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             
             [error_Alert show];
             [Activity stopAnimating];
             [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
             
         }];
        [request startAsynchronous];
        
        
        
    }
}



- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
   
    
    if (alertView==delete_alert)
    {
        NSLog(@"DEL:%@",idArray[delIndex]);
        
        //NSLog(@"Sele:%@",[abc objectAtIndex:0]);
        
        NSString *delIDStr=[NSString stringWithFormat:@"%@",idArray[delIndex]];
        if (buttonIndex==1)
        {
            /*
            NSString *remove_file=[NSString stringWithFormat:@"%@.epub",delIDStr];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectoryPath = [paths objectAtIndex:0];
            
            // Delete the file using NSFileManager
            NSFileManager *fileManager = [NSFileManager defaultManager];
            [fileManager removeItemAtPath:[documentsDirectoryPath stringByAppendingPathComponent:remove_file] error:nil];
            */
            NSUserDefaults *userde=[NSUserDefaults standardUserDefaults];
            NSString *dbUsername=[userde objectForKey:@"USERNAME_EMAIL"];
            
            
            Dataware *db1=[[Dataware alloc]initDataware];
            NSString *strr=[NSString stringWithFormat:@"DELETE from tb_library where bid=? and user_id=?"];
            sqlite3_stmt *stmt=[db1 OpenSQL:[strr UTF8String]];
            if(stmt != nil)
            {
                //sqlite3_bind_int(stmt,1,mobile_str);
                sqlite3_bind_text(stmt,1,[delIDStr  UTF8String],-1,SQLITE_TRANSIENT);
                sqlite3_bind_text(stmt,2,[dbUsername  UTF8String],-1,SQLITE_TRANSIENT);
                sqlite3_step(stmt);sqlite3_reset(stmt);
                
            }
            
            [db1 CloseSQL];
            
            
            [self getFromDB];

        }
        else
        {
            
        }
    }
     
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    if (tableView==searchTable)
    {
        return [searchIDArray count];
    }
    else
    return [idArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    if (tableView==searchTable)
    {
        SearchCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            
            NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"SearchCell" owner:nil options:nil];
            
            for (UIView *view in views)
            {
                if([view isKindOfClass:[UITableViewCell class]])
                {
                    cell = (SearchCell*)view;
                    //cell.img=@"date.png";
                    
                }
            }
        }
        
         cell.selectionStyle=UITableViewCellSelectionStyleGray;
        
        [cell.img setImageWithURL:[NSURL URLWithString:searchImageArray[indexPath.row]]
                        placeholderImage:[UIImage imageNamed:@"temp.jpg"]];

        
       // cell.img.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",searchImageArray[indexPath.row]]];
        cell.title.text=searchStringArray[indexPath.row];
        cell.category.text=searchmetadataArray[indexPath.row];
        return cell;
    }
    else
    {
    
    LibraryCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"LibraryCell" owner:nil options:nil];
        
        for (UIView *view in views)
        {
            if([view isKindOfClass:[UITableViewCell class]])
            {
                cell = (LibraryCell*)view;
                //cell.img=@"date.png";
                
            }
        }
    }
   
        cell.selectionStyle=UITableViewCellSelectionStyleGray;
        
        [cell.img setImageWithURL:[NSURL URLWithString:imageArray[indexPath.row]]
                 placeholderImage:[UIImage imageNamed:@"temp.jpg"]];
        
    // cell.img.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",imageArray[indexPath.row]]];
    cell.title.text=textArray[indexPath.row];
    cell.category.text=metadataArray[indexPath.row];
        
        if (downloading_id==12345)
        {
            cell.progress.hidden=YES;
        }
        else
        {
            if (downloading_id==indexPath.row)
            {
                cell.progress.hidden=NO;
                //[request setDownloadProgressDelegate:cell.progress];
            }
            else
            {
                cell.progress.hidden=YES;
                //[request setDownloadProgressDelegate:cell.progress];
            }
            
            
            
        }

        
        
        return cell;
        
    }
    
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView==bookmarkTable) {
        return 118;
    }
    else
    {
        return 100;
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Select %d",indexPath.row);
    
    //[_popoverController_bookmark dismissPopoverAnimated:YES];
    
    searchView.hidden=YES;
    
    if (tableView==searchTable)
    {
        
        
        
        
        
        
        
        NSString *send_book_id=[NSString stringWithFormat:@"%@",searchIDArray[indexPath.row]];
        NSString *send_book_name=[NSString stringWithFormat:@"%@",searchStringArray[indexPath.row]];
        NSString *epubFileDownloadURL=[NSString stringWithFormat:@"%@",searchEpubArray[indexPath.row]];
        
        
        NSLog(@"BOOK ID:%@",send_book_id);
        NSLog(@"BOOK NAME:%@",send_book_name);
        NSLog(@"BOOK URL:%@",epubFileDownloadURL);
        
        
        NSUserDefaults *passValues=[NSUserDefaults standardUserDefaults];
        [passValues setObject:send_book_id forKey:@"BOOK_ID"];
        [passValues setObject:send_book_id forKey:@"BOOK_EPUB"];
        [passValues setObject:send_book_name forKey:@"BOOK_NAME"];
        
        
        NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *epub_name=[NSString stringWithFormat:@"%@.epub",send_book_id];
        NSString* foofile = [documentsPath stringByAppendingPathComponent:epub_name];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
        
        
        if (fileExists)
        {
            NSLog(@"EXISTS");
            
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.height == 480)
                {
                    // iPhone Classic
                    
                    NSLog(@"OLD IPHONE");
                }
                if(result.height == 568)
                {
                    // iPhone 5
                    EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EpubView~iPhone5" bundle:nil];
                    
                    //epub.epubName=@"7";
                    
                    epub.epubName=send_book_id;
                    
                    [self presentModalViewController:epub animated:NO];            }
            }
            else
            {
                NSLog(@"iPAd");
                
                EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EPubView" bundle:nil];
                epub.epubName=send_book_id;
                // epub.epubName=@"7777";
                
                [self presentModalViewController:epub animated:NO];
            }
            
        }
        else
        {
            NSLog(@"NOT EXISTS");
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
            
            
            threadProgressView.hidden=NO;
            
            
            
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSURL *url=[NSURL URLWithString:epubFileDownloadURL];
            
            
            NSLog(@"Dowload Start");
            
            __block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
            [request setDownloadProgressDelegate:threadProgressView];
            
            [request setShouldContinueWhenAppEntersBackground:YES];
             [request setTimeOutSeconds:240];
            
            [request setCompletionBlock:^{
                // Use when fetching text data
                
                NSString *responseString = [request responseString];
                
                
                
                // Use when fetching binary data
                NSData *responseData = [request responseData];
                
                NSString *epub_name=[NSString stringWithFormat:@"%@.epub",send_book_id];
                
                
                NSString *filePath = [documentsDirectory stringByAppendingPathComponent:epub_name];
                
                [responseData writeToFile:filePath atomically:YES];
                
                NSLog(@"Dowload Completes");
                
                threadProgressView.hidden=YES;
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
                // [self Load:nil];
                
                
                NSUserDefaults *passValues=[NSUserDefaults standardUserDefaults];
                [passValues setObject:send_book_id forKey:@"BOOK_ID"];
                [passValues setObject:send_book_id forKey:@"BOOK_EPUB"];
                [passValues setObject:send_book_name forKey:@"BOOK_NAME"];
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    CGSize result = [[UIScreen mainScreen] bounds].size;
                    if(result.height == 480)
                    {
                        // iPhone Classic
                        
                        EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EpubView~iPhone" bundle:nil];
                        
                        //epub.epubName=@"7";
                        
                        epub.epubName=send_book_id;
                        
                        [self presentModalViewController:epub animated:NO];
                        
                        NSLog(@"OLD IPHONE");
                    }
                    if(result.height == 568)
                    {
                        // iPhone 5
                        EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EpubView~iPhone5" bundle:nil];
                        
                        //epub.epubName=@"7";
                        
                        epub.epubName=send_book_id;
                        
                        [self presentModalViewController:epub animated:NO];            }
                }
                else
                {
                    NSLog(@"iPAd");
                    
                    EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EPubView" bundle:nil];
                    epub.epubName=send_book_id;
                    // epub.epubName=@"7777";
                    
                    [self presentModalViewController:epub animated:NO];
                }
                
                
            }];
            [request setFailedBlock:^
             {
                 // NSError *error = [request error];
                 
                 
                 UIAlertView *error_Alert=[[UIAlertView alloc]initWithTitle:@"Download Error" message:[NSString stringWithFormat:@"%@",[request error]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 
                 [error_Alert show];
                 
                 
             }];
            [request startAsynchronous];
            
            
        }
    }
    else
    {
        
        
        
            NSString *send_book_id=[NSString stringWithFormat:@"%@",idArray[indexPath.row]];
            NSString *send_book_name=[NSString stringWithFormat:@"%@",textArray[indexPath.row]];
            NSString *epubFileDownloadURL=[NSString stringWithFormat:@"%@",epubArray[indexPath.row]];
        
        
            NSLog(@"BOOK ID:%@",send_book_id);
            NSLog(@"BOOK NAME:%@",send_book_name);
            NSLog(@"BOOK URL:%@",epubFileDownloadURL);
        
        
            NSUserDefaults *passValues=[NSUserDefaults standardUserDefaults];
            [passValues setObject:send_book_id forKey:@"BOOK_ID"];
            [passValues setObject:send_book_id forKey:@"BOOK_EPUB"];
            [passValues setObject:send_book_name forKey:@"BOOK_NAME"];
            
            
            NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *epub_name=[NSString stringWithFormat:@"%@.epub",send_book_id];
            NSString* foofile = [documentsPath stringByAppendingPathComponent:epub_name];
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
            
            
            if (fileExists)
            {
                NSLog(@"EXISTS");
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    CGSize result = [[UIScreen mainScreen] bounds].size;
                    if(result.height == 480)
                    {
                        // iPhone Classic
                        
                        NSLog(@"OLD IPHONE");
                        
                        EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EpubView~iPhone" bundle:nil];
                        
                        //epub.epubName=@"7";
                        
                        epub.epubName=send_book_id;
                        
                        [self presentModalViewController:epub animated:NO];
                    }
                    if(result.height == 568)
                    {
                        // iPhone 5
                        EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EpubView~iPhone5" bundle:nil];
                        
                        //epub.epubName=@"7";
                        
                        epub.epubName=send_book_id;
                        
                        [self presentModalViewController:epub animated:NO];            }
                }
                else
                {
                    NSLog(@"iPAd");
                    
                    EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EPubView" bundle:nil];
                    epub.epubName=send_book_id;
                    // epub.epubName=@"7777";
                    
                    [self presentModalViewController:epub animated:NO];
                }
                
            }
            else
            {
                NSLog(@"NOT EXISTS");
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                
               
                
                
                downloading_id=indexPath.row;
                
                //threadProgressView.hidden=NO;
                
                
                
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSURL *url=[NSURL URLWithString:epubFileDownloadURL];
                
                
                NSLog(@"Dowload Start");
                
                request = [ASIHTTPRequest requestWithURL:url];
               
                [self.collectionView reloadData];
                [bookmarkTable reloadData];
                
                
                //[request setDownloadProgressDelegate:threadProgressView];
                
                [request setShouldContinueWhenAppEntersBackground:YES];
                 [request setTimeOutSeconds:240];
                
                [request setCompletionBlock:^{
                    // Use when fetching text data
                    
                    NSString *responseString = [request responseString];
                    
                    
                    
                    // Use when fetching binary data
                    NSData *responseData = [request responseData];
                    
                    NSString *epub_name=[NSString stringWithFormat:@"%@.epub",send_book_id];
                    
                    
                    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:epub_name];
                    
                    [responseData writeToFile:filePath atomically:YES];
                    
                    NSLog(@"Dowload Completes");
                    
                    threadProgressView.hidden=YES;
                    
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    
                    // [self Load:nil];
                    
                    
                    NSUserDefaults *passValues=[NSUserDefaults standardUserDefaults];
                    [passValues setObject:send_book_id forKey:@"BOOK_ID"];
                    [passValues setObject:send_book_id forKey:@"BOOK_EPUB"];
                    [passValues setObject:send_book_name forKey:@"BOOK_NAME"];
                    
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                    {
                        CGSize result = [[UIScreen mainScreen] bounds].size;
                        if(result.height == 480)
                        {
                            // iPhone Classic
                            
                            NSLog(@"OLD IPHONE");
                        }
                        if(result.height == 568)
                        {
                            // iPhone 5
                            EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EpubView~iPhone5" bundle:nil];
                            
                            //epub.epubName=@"7";
                            
                            epub.epubName=send_book_id;
                            
                            [self presentModalViewController:epub animated:NO];            }
                    }
                    else
                    {
                        NSLog(@"iPAd");
                        
                        EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EPubView" bundle:nil];
                        epub.epubName=send_book_id;
                        // epub.epubName=@"7777";
                        
                        [self presentModalViewController:epub animated:NO];
                    }
                    
                    
                }];
                [request setFailedBlock:^
                 {
                     // NSError *error = [request error];
                     
                     
                     UIAlertView *error_Alert=[[UIAlertView alloc]initWithTitle:@"Download Error" message:[NSString stringWithFormat:@"%@",[request error]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     
                     [error_Alert show];
                     
                     
                 }];
                [request startAsynchronous];
                
                
            }
            
                                 
            
        

    }
    
   }

- (IBAction)Store:(id)sender
{
    Reachability1 *reach=[Reachability1 reachabilityWithHostName:@"www.souq.com"];
	
    NetworkStatus internetStatus=[reach currentReachabilityStatus];
	
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))
		
	{
		
        NSLog(@"Network is unreachable 12.");
    }
    else
    {
    WebViewController *epub=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    epub.comingView=@"LIBRARY";
    [self presentModalViewController:epub animated:NO];
    }
    
   
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */







- (IBAction)List:(id)sender
{
    
    [gridButton setImage:[UIImage imageNamed:@"libGCviewLands.png"] forState:UIControlStateNormal];
    [listButton setImage:[UIImage imageNamed:@"libLviewLandsACT.png"] forState:UIControlStateNormal];

    
    bookmarkTable.hidden=NO;
    collectionView.hidden=YES;
    
    if (editID==1)
    {
        editID=0;
        [collectionView reloadData];
        [editButton setTitle:@"Edit" forState:UIControlStateNormal];
    }
    
    
}
- (IBAction)Grid:(id)sender
{
    
    [gridButton setImage:[UIImage imageNamed:@"libGviewLandsACT.png"] forState:UIControlStateNormal];
    [listButton setImage:[UIImage imageNamed:@"libviewCLLands.png"] forState:UIControlStateNormal];

    
    bookmarkTable.hidden=YES;
    collectionView.hidden=NO;
    
    if (editID==1)
    {
        editID=0;
        [collectionView reloadData];
        [editButton setTitle:@"Edit" forState:UIControlStateNormal];
    }
    
}
- (IBAction)Edit:(id)sender
{
    
    [gridButton setImage:[UIImage imageNamed:@"libGviewLandsACT.png"] forState:UIControlStateNormal];
    [listButton setImage:[UIImage imageNamed:@"libviewCLLands.png"] forState:UIControlStateNormal];

    
    bookmarkTable.hidden=YES;
    collectionView.hidden=NO;
    
    if (editID==0)
    {
         editID=1;
        [collectionView reloadData];
        [editButton setTitle:@"Done" forState:UIControlStateNormal];
        
      
    }
    else
    {
        
        editID=0;
        [collectionView reloadData];
        [editButton setTitle:@"Edit" forState:UIControlStateNormal];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Load:(id)sender {
    
    EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EPubView" bundle:nil];
    
    
    
    [self presentModalViewController:epub animated:NO];
    
}

- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
  
    if ([searchTextField.text isEqualToString:@""]) {
        
        [textField resignFirstResponder];
    }
    else
    {
   
    searchView.hidden=NO;
    
    }
    
    searchStringArray=[[NSMutableArray alloc]init];
    searchIDArray=[[NSMutableArray alloc]init];
    searchImageArray=[[NSMutableArray alloc]init];
    searchEpubArray=[[NSMutableArray alloc]init];
    
    searchcidArray=[[NSMutableArray alloc]init];
    searchmetadataArray=[[NSMutableArray alloc]init];
    searchisDownloadArray=[[NSMutableArray alloc]init];
    searchauthorArray=[[NSMutableArray alloc]init];
    searcisDownloadebleArray=[[NSMutableArray alloc]init];
    searchpreview_url_array=[[NSMutableArray alloc]init];
    searchisPreviewedArray=[[NSMutableArray alloc]init];
    
    NSLog(@"TEXT:%@",searchTextField.text);
    
    NSString *textString=searchTextField.text;
    
    for (int i=0; i<[textArray count]; i++)
    {
        
        NSString *textArrayString=textArray[i];
        
        NSLog(@"TEXT ARRAY:%@",textArrayString);
        
        textArrayString=[textArrayString lowercaseString];
        textString=[textString lowercaseString];
        
        if ([textArrayString rangeOfString:textString].location != NSNotFound)
        {
            [searchStringArray addObject:textArray[i]];
            [searchIDArray addObject:idArray[i]];
            [searchImageArray addObject:imageArray[i]];
            [searchEpubArray addObject:epubArray[i]];
            
            [searchcidArray addObject:cidArray[i]];
            [searchmetadataArray addObject:metadataArray[i]];
            [searchisDownloadArray addObject:isDownloadArray[i]];
            [searchauthorArray addObject:authorArray[i]];
            [searcisDownloadebleArray addObject:isDownloadebleArray[i]];
            [searchisPreviewedArray addObject:isPreviewedArray[i]];
            [searchpreview_url_array addObject:preview_url_array[i]];
        }

        
    }
    
    NSLog(@"Search String:%@",searchStringArray);
    NSLog(@"Search ID:%@",searchIDArray);
    
    [searchTable reloadData];
    
   
    
	[textField resignFirstResponder];
	return YES;
    
}

-(BOOL)shouldAutorotate{
    return YES;
}


-(NSInteger)supportedInterfaceOrientations{
    
    // [[_searchBar.subviews objectAtIndex:0] removeFromSuperview];
    
       
    if (UIDeviceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]))
    {
//        storeButton.frame=CGRectMake(10, 104, 68, 37);
//        
//        
//        editButton.frame=CGRectMake(685, 104, 68, 37);
//        
//        
//        
//         gridButton.frame=CGRectMake(605, 104, 36, 37);
//        listButton.frame=CGRectMake(642, 104, 39, 37);
//        
//        
//        
//        _topLogo.frame=CGRectMake(0, 0, 206, 76);
//        
//        
//        
//        
////        searchTextField.frame=CGRectMake(125, 104, 230, 37);
////        _searchImage.frame=CGRectMake(120, 104, 238, 37);
////        _searchImage.image=[UIImage imageNamed:@"topbarsearchPort1.png"];
//        
//        searchTextField.frame=CGRectMake(105, 105, 476, 37);
//        _searchImage.frame=CGRectMake(90, 105, 500, 37);
//        _searchImage.image=[UIImage imageNamed:@"topsearchLands.png"];
//        
//        topImageView.frame=CGRectMake(0, -38, 768, 184);
//        _bottomView.frame=CGRectMake(0, 920, 768, 100);
//        collectionView.frame=CGRectMake(0, 140, 768, 778);
//        topView.frame=CGRectMake(0, 0, 768, 146);
//        _topLogo.frame=CGRectMake(0, 16, 206, 76);
//        topImageView.image=[UIImage imageNamed:@"libtopbarPort.png"];
//        
//        _bottomLabel.frame=CGRectMake(560, 40, 176, 21);
        
    }
    
    else
    {
//         _bottomView.frame=CGRectMake(0, 670, 1024, 100);
//        topImageView.image=[UIImage imageNamed:@"libtopbarLands"];
//         topImageView.frame=CGRectMake(0, -30, 1024, 175);
//        topView.frame=CGRectMake(0, 0, 1024, 145);
//
//       storeButton.frame=CGRectMake(20, 105, 68, 37);
//        
//        editButton.frame=CGRectMake(930, 105, 68, 37);
//        
//        gridButton.frame=CGRectMake(850, 105, 36, 37);
//        listButton.frame=CGRectMake(887, 105, 39, 37);
//        
//        
//        _topLogo.frame=CGRectMake(16, 12, 206, 76);
//        
//        searchTextField.frame=CGRectMake(125, 105, 476, 37);
//         _searchImage.frame=CGRectMake(120, 105, 500, 37);
//        _searchImage.image=[UIImage imageNamed:@"topsearchLands.png"];
//        
//         _bottomLabel.frame=CGRectMake(800, 40, 176, 21);
     
    }
    return UIInterfaceOrientationMaskAll;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)orientation
                                duration:(NSTimeInterval)duration
{
    
    if (UIDeviceOrientationIsPortrait(orientation)) {
        
    }
    
    else {
        
    }
}

- (IBAction)Terms:(id)sender
{
    WebViewController *epub=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    epub.comingView=@"TERMS";
    [self presentModalViewController:epub animated:NO];
}
- (IBAction)FAQ:(id)sender
{
    WebViewController *epub=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    epub.comingView=@"FAQ";
    [self presentModalViewController:epub animated:NO];
}
- (IBAction)About:(id)sender
{
    WebViewController *epub=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    epub.comingView=@"ABOUT";
    [self presentModalViewController:epub animated:NO];
}
- (IBAction)Contact:(id)sender
{
    WebViewController *epub=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    epub.comingView=@"CONTACT";
    [self presentModalViewController:epub animated:NO];
}

- (IBAction)Referesh:(id)sender
{
     [self parseJSON];
}


@end
