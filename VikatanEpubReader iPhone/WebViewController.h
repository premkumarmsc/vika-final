//
//  ViewController.h
//  Vikatan
//
//  Created by ephronsystems on 6/4/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FbGraph.h"
#import <GooglePlus/GooglePlus.h>
#import <StoreKit/StoreKit.h>
#import "AePubReaderAppDelegate.h"

@interface WebViewController : GAITrackedViewController<GPPSignInDelegate,SKPaymentTransactionObserver,SKProductsRequestDelegate>
{
     FbGraph *fbGraph;
    SKProductsRequest *productsRequest;
    NSArray *validProducts;
    UIActivityIndicatorView *activityIndicatorView;
}
@property(nonatomic,retain) IBOutlet UIActivityIndicatorView *Activity;
- (BOOL)canMakePurchases;
- (void)fetchAvailableProducts;
- (void)purchaseMyProduct:(SKProduct*)product;
- (IBAction)purchase:(id)sender;
@property(nonatomic,retain)IBOutlet UIWebView *web_view;
@property(nonatomic,retain)NSString *comingView;
@property(nonatomic,retain)NSString *comingFrom;
@property (retain, nonatomic) IBOutlet GPPSignInButton *signInButton;
// A label to display the result of the sign-in action.
@property (retain, nonatomic) IBOutlet UILabel *signInAuthStatus;
// A label to display the signed-in user's display name.
@property (retain, nonatomic) IBOutlet UILabel *signInDisplayName;
// A button to sign out of this application.
@property (retain, nonatomic) IBOutlet UIButton *signOutButton;
// A button to disconnect user from this application.
@property (retain, nonatomic) IBOutlet UIButton *disconnectButton;
// A switch for whether to request
// https://www.googleapis.com/auth/userinfo.email scope to get user's email
// address after the sign-in action.
@property (retain, nonatomic) IBOutlet UISwitch *userinfoEmailScope;

// Called when the user presses the "Sign out" button.
- (IBAction)signOut:(id)sender;
// Called when the user presses the "Disconnect" button.
- (IBAction)disconnect:(id)sender;
// Called when the user toggles the
// https://www.googleapis.com/auth/userinfo.email scope.
- (IBAction)userinfoEmailScopeToggle:(id)sender;

@end
