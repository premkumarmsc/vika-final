//
//  File_Copy.h
//  Demo_CollectionView
//
//  Created by ephronsystems on 6/1/13.
//  Copyright (c) 2013 ephronsystems. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface File_Copy : NSObject
-(BOOL)copyFileToDocumentsFolder:(NSString *)fileName;
-(BOOL)copyFileToDocumentsFolder:(NSString *)fileName pass:(NSString *)password;
-(BOOL)downloadFileToDocumentsFolder:(NSString *)fileURL zipName:(NSString *)zipFileName folder:(NSString *)folderName;
+(BOOL)alreadyFileExists;
+(BOOL)iSIPhone;
+(BOOL)iSIPhone5;

@end
