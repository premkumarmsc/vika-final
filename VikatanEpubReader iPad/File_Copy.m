//
//  File_Copy.m
//  Demo_CollectionView
//
//  Created by ephronsystems on 6/1/13.
//  Copyright (c) 2013 ephronsystems. All rights reserved.
//

#import "File_Copy.h"
#import "ZipArchive.h"


@implementation File_Copy

-(BOOL)copyFileToDocumentsFolder:(NSString *)fileName
{
    
    BOOL copySucceeded = NO;
    
    // Get our document path.
    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [searchPaths objectAtIndex:0];
    
    // Get the full path to our file.
    
       
    NSString *filePath = [documentPath stringByAppendingPathComponent:fileName];
    
    //NSLog(@"copyFromBundle - checking for presence of \"%@\"...", fileName);
    
    // Get a file manager
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // Does the database already exist? (If not, copy it from our bundle)
    if(![fileManager fileExistsAtPath:filePath]) {
        
        // Get the bundle location
        NSString *bundleDBPath = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
        
        // Copy the DB to our document directory.
        copySucceeded = [fileManager copyItemAtPath:bundleDBPath
                                             toPath:filePath
                                              error:nil];
        
        if(!copySucceeded) {
            //NSLog(@"copyFromBundle - Unable to copy \"%@\" to document directory.", fileName);
        }
        else {
            //NSLog(@"copyFromBundle - Succesfully copied \"%@\" to document directory.", fileName);
        }
        
    }
    else {
        //NSLog(@"copyFromBundle - \"%@\" already exists in document directory - ignoring.", fileName);
    }
    
    
     return [self fileCompressor:fileName];
    
}
+(BOOL)iSIPhone
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        return YES;
    }
    else
    {
        NSLog(@"iPAd");
        
        return NO;
    }

}

+(BOOL)iSIPhone5
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.height == 480)
        {
            // iPhone Classic
            
            return NO;
        }
        if(result.height == 568)
        {
            // iPhone 5
            return YES;
        }
    }
}

-(BOOL)copyFileToDocumentsFolder:(NSString *)fileName pass:(NSString *)password
{
    
    BOOL copySucceeded = NO;
    
    // Get our document path.
    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [searchPaths objectAtIndex:0];
    
    // Get the full path to our file.
    
    
    NSString *filePath = [documentPath stringByAppendingPathComponent:fileName];
    
    //NSLog(@"copyFromBundle - checking for presence of \"%@\"...", fileName);
    
    // Get a file manager
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // Does the database already exist? (If not, copy it from our bundle)
    if(![fileManager fileExistsAtPath:filePath]) {
        
        // Get the bundle location
        NSString *bundleDBPath = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
        
        // Copy the DB to our document directory.
        copySucceeded = [fileManager copyItemAtPath:bundleDBPath
                                             toPath:filePath
                                              error:nil];
        
        if(!copySucceeded) {
            //NSLog(@"copyFromBundle - Unable to copy \"%@\" to document directory.", fileName);
        }
        else {
            //NSLog(@"copyFromBundle - Succesfully copied \"%@\" to document directory.", fileName);
        }
        
    }
    else {
        //NSLog(@"copyFromBundle - \"%@\" already exists in document directory - ignoring.", fileName);
    }
    
    
    return [self fileCompressor:fileName pass:password];
    
}

/*
-(BOOL)downloadFileToDocumentsFolder:(NSString *)fileURL zipName:(NSString *)zipFileName folder:(NSString *)folderName
{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
     NSURL *url=[NSURL URLWithString:fileURL];
     //NSLog(@"Dowload Start");
    __block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request setShouldContinueWhenAppEntersBackground:YES];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [request setCompletionBlock:^{
        // Use when fetching text data
        
        NSString *responseString = [request responseString];
        
        
        
        // Use when fetching binary data
        NSData *responseData = [request responseData];
        
        
        
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:zipFileName];
        
        [responseData writeToFile:filePath atomically:YES];
        
        //NSLog(@"Dowload Completes");
        
        
        
        //NSLog(@"UnZip Start");
        
        NSString *output = [documentsDirectory stringByAppendingPathComponent:folderName];
        
        ZipArchive* za = [[ZipArchive alloc] init];
        
        if( [za UnzipOpenFile:filePath] )
        {
            
            if( [za UnzipFileTo:output overWrite:YES] != NO )
            {
                //unzip data success
                //do something
                
                //NSLog(@"UnZip Finished");
            }
            
            
            
            [za UnzipCloseFile];
        }
       [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        
        
        
    }];
    [request setFailedBlock:^{
        // NSError *error = [request error];
    }];
    [request startAsynchronous];
    
    
    return YES;
}

*/


-(BOOL)fileCompressor:(NSString *)fileName pass:(NSString *)password
{
    {
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];
        
        
        //NSLog(@"UnZip Start");
        
        
        
        ZipArchive* za = [[ZipArchive alloc] init];
        
       
        
        if( [za UnzipOpenFile:filePath Password:password] )
        {
            
            if( [za UnzipFileTo:documentsDirectory overWrite:YES] != NO )
            {
                //unzip data success
                //do something
                
                //NSLog(@"UnZip Finished");
                
                return YES;
            }
            else
                return NO;
            
            [za UnzipCloseFile];
        }
        
    }
}



-(BOOL)fileCompressor:(NSString *)fileName
{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    
    //NSLog(@"UnZip Start");
    
    
    
    ZipArchive* za = [[ZipArchive alloc] init];
    
    if( [za UnzipOpenFile:filePath] )
    {
        
        if( [za UnzipFileTo:documentsDirectory overWrite:YES] != NO )
        {
            //unzip data success
            //do something
            
            //NSLog(@"UnZip Finished");
            
            return YES;
        }
        else
            return NO;
        
        [za UnzipCloseFile];
    }
    
}


+(BOOL)alreadyFileExists
{
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* foofile = [documentsPath stringByAppendingPathComponent:@"Demo.zip"];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
    
    return fileExists;
        
}
@end
