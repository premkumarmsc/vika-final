//
//  DetailViewController.h
//  AePubReader
//
//  Created by PREMKUMAR on 04/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZipArchive.h"
#import "EPub.h"
#import "Chapter.h"

@class SearchResultsViewController;
@class SearchResult;

@interface EPubViewController : GAITrackedViewController <UIWebViewDelegate, ChapterDelegate, UISearchBarDelegate,UIAlertViewDelegate,UIScrollViewDelegate,UIActionSheetDelegate> {
    
    UIToolbar *toolbar;
        
	UIWebView *webView;
    
    UIButton* chapterListButton;
	
	UIBarButtonItem* decTextSizeButton;
	UIBarButtonItem* incTextSizeButton;
    
    UISlider* pageSlider;
    UILabel* currentPageLabel;
			
	EPub* loadedEpub;
	int currentSpineIndex;
	int currentPageInSpineIndex;
	int pagesInCurrentSpineCount;
	int currentTextSize;
	int totalPagesCount;
    
    BOOL epubLoaded;
    BOOL paginating;
    BOOL searching;
    
   
    
    
    UIPopoverController* chaptersPopover;
    UIPopoverController* searchResultsPopover;

    SearchResultsViewController* searchResViewController;
    SearchResult* currentSearchResult;
}

@property (nonatomic, retain) IBOutlet UISlider *mySlider;
- (IBAction) sliderValueChanged:(id)sender;

@property (nonatomic,retain) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic,retain) NSString *comingView;
@property (nonatomic,retain) IBOutlet UIButton *doneButton;

-(IBAction) segmentedControlIndexChanged;
- (IBAction) showChapterIndex:(id)sender;
- (IBAction) increaseTextSizeClicked:(id)sender;
- (IBAction) decreaseTextSizeClicked:(id)sender;
- (IBAction) slidingStarted:(id)sender;
- (IBAction) slidingEnded:(id)sender;
- (IBAction) doneClicked:(id)sender;

-(IBAction)dark:(id)sender;
-(IBAction)light:(id)sender;
-(IBAction)gray:(id)sender;


-(IBAction)bookMark:(id)sender;

-(IBAction)fullscreen:(id)sender;


- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex highlightSearchResult:(SearchResult*)theResult;

- (void) loadEpub:(NSURL*) epubURL;

@property (nonatomic, retain) EPub* loadedEpub;

@property (nonatomic, retain) SearchResult* currentSearchResult;

@property (nonatomic, retain) IBOutlet UIToolbar *toolbar;

@property (nonatomic, retain) IBOutlet UIWebView *webView;

@property (nonatomic, retain) IBOutlet UIButton *chapterListButton;
@property (nonatomic, retain) IBOutlet UIButton *libraryButton;
@property (nonatomic, retain) IBOutlet UIView *topView;
@property (nonatomic, retain) IBOutlet UIImageView *topImage;
@property (nonatomic, retain) IBOutlet UIImageView *logoImage;
@property (nonatomic, retain) IBOutlet UIButton *addBookmarkbutton;
@property (nonatomic, retain) IBOutlet UIButton *fullscreenButton;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;

@property (nonatomic, retain) IBOutlet UILabel *titleLabelFull;

@property (nonatomic, retain) IBOutlet UIBarButtonItem *decTextSizeButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *incTextSizeButton;

@property (nonatomic, retain) IBOutlet UISlider *pageSlider;
@property (retain, nonatomic) IBOutlet UIView *bookmarkView;
@property (nonatomic, retain) IBOutlet UILabel *currentPageLabel;
@property (retain, nonatomic) IBOutlet UITableView *bookmarkTable;
@property (retain, nonatomic)UIPopoverController *popoverController_bookmark;
@property BOOL searching;
- (IBAction)viewBookmark:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *viewbookmarkBtn;
@property (retain, nonatomic) IBOutlet UIButton *settingBtn;
@property(nonatomic,retain)UIAlertView *alertView;
@property (retain, nonatomic) IBOutlet UIView *settingsView;
@property (retain, nonatomic) IBOutlet UIImageView *imageViewAnimate;;
@property (retain, nonatomic)UIPopoverController *settingsPop;
- (IBAction)settingsButton:(id)sender;
@property (retain, nonatomic) IBOutlet UISearchBar *searchBar;


@property(nonatomic,retain)NSString *epubName;

@end
