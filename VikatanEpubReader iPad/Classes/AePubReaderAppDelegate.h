//
//  AePubReaderAppDelegate.h
//  AePubReader
//
//  Created by PREMKUMAR on 04/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook.h"

//@class EPubViewController;
@class WebViewController;
@class ViewController;
@interface AePubReaderAppDelegate : NSObject <UIApplicationDelegate,FBSessionDelegate>
{
    
    UIWindow *window;
    ViewController *detailViewController1;
    WebViewController *detailViewController;
}
@property (nonatomic, strong) Facebook *facebook;
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet ViewController *detailViewController1;
//@property (nonatomic, retain) IBOutlet EPubViewController *detailViewController;
@property (nonatomic, retain) IBOutlet WebViewController *detailViewController;
@end
